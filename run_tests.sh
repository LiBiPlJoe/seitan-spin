#!/bin/bash

# set DS_HOST to "localhost" for local runs, "docker" for CI runs.
# set DB_HOST_OVERRIDE to "localhost" for local runs, "docker" for CI runs.
echo "Checking hostname $(hostname -f)..."
domain=$(hostname -d)
echo " domain is $domain"
if [[ $domain == "localdomain" || $domain == "local" ]]; then
  export DS_HOST=localhost
  export DB_HOST_OVERRIDE=localhost
fi

export DS_PORT=27998
export DB_PORT_OVERRIDE=27999
export TEST_DEBUG=false
export TELNET_TRACE=false
export CUCUMBER_PUBLISH_QUIET=true

export DB_USERNAME=sa
export DB_PASSWORD=Rapunz3l!
export DB_DATASERVER=tcp:sql,1433 # How the game server will contact the database.

export SQLSERVR_SA_PASSWORD=${DB_PASSWORD}
export SQLSERVR_DB_NAME=minimal

cp docker-compose-prod.yml docker-compose.yml

# Clear and recreate the dir for the gameserver volume.
rm -rf gameserver
mkdir gameserver

# Make sure the Docker Compose environment is valid.
docker compose down
docker compose up --quiet-pull --detach

# Make sure we have all of the right gems.
bundle install

set -o pipefail # So tee doesn't overwrite a failure code
bundle exec cucumber $1 --order random --retry 3 --tags 'not @WIP' --tags 'not @slow' --tags 'not @bug' \
 --format html --out cucumber.html \
 --format usage --out usage.out \
 --format pretty | tee cucumber.out
