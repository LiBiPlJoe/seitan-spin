FROM registry.gitlab.com/libipljoe/drag-spin-exp:latest

RUN apt-get update -qq
RUN apt list | grep ruby
RUN apt-get install -y ruby ruby-dev ubuntu-dev-tools
RUN gem install bundler -v 2.3.26

COPY Gemfile Gemfile.lock ./

RUN apt-get install -y freetds-dev freetds-bin
RUN apt-get install -y mssql-tools

COPY . .
COPY docker/ .

RUN bundle install

RUN chmod +x ./run_tests.sh

ENTRYPOINT [""]
