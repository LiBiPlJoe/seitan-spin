@kill_server_after
Feature: Temporary scenarios

Scenario: Multiple character effects stack correctly
	Given I use the "minimal" database
	And I add player and character "TestSubject01"
	And I inflict fear on the character for "2" turns
	And I blind the character for "4" turns
	And the server is started
	When I log on as "TestSubject01"
	And I enter the game
	Then I saw a message "You are scared!"
	And after "2" turns I no longer see the message "You are scared!"
	Then I saw a message "You are blind!"
	And after "2" turns I no longer see the message "You are blind!"
