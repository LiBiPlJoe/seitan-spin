@kill_server_after
Feature: PlayerEffects

## Dog_Follow
# TODO

## Balm

Scenario: Balm effect processes correctly when limited by duration
    Given I use the "minimal" database as-is
    And I add player and character "TestBalm01"
    And I set the character's max HP to "30"
    And I set the character's current HP to "1"
    And I give the character a "balm" effect of "20" for "3" turns
    And the server is started
    When I log on as "TestBalm01"
    And I enter the game
    # 1 + 10 (initial effect) = 11 (amount now 10, duration now 3)
    And after "0" turns I have a current HP of "16"
    # 11 + 5 (effect on initial turn) = 16 (amount now 5, duration now 2)
    And after "1" turns I have a current HP of "18"
    # 16 + 2 (effect) = 18 (amount now 3, duration now 1)
    And after "1" turns I have a current HP of "19"
    # 18 + 1 (effect) = 19 (amount now 2, duration now 0)
    And after "1" turns I have a current HP of "22"
    # 19 + 2 (effect) = 21 (effect completed)
    # 21 + 1 (third round healing) = 22
    And after "1" turns I have a current HP of "22"
    And after "1" turns I have a current HP of "22"
    And after "1" turns I have a current HP of "23"
    # 22 + 1 (third round healing) = 23

Scenario: Balm effect processes correctly when limited by effect
    Given I use the "minimal" database as-is
    And I add player and character "TestBalm02"
    And I set the character's max HP to "30"
    And I set the character's current HP to "1"
    And I give the character a "balm" effect of "10" for "4" turns
    And the server is started
    When I log on as "TestBalm02"
    And I enter the game
    # 1 + 5 (initial effect) = 6 (amount now 5, duration now 4)
    And after "0" turns I have a current HP of "8"
    # 6 + 2 (effect on initial turn) = 8 (amount now 3, duration now 3)
    And after "1" turns I have a current HP of "9"
    # 8 + 1 (effect) = 9 (amount now 2, duration now 2)
    And after "1" turns I have a current HP of "10"
    # 9 + 1 (effect) = 10 (amount now 1, duration now 1)
    And after "1" turns I have a current HP of "11"
    # 11 + 0 (effect) = 11 (amount still 1, duration now 0)
    # 11 + 1 (third round healing) = 12
    And after "1" turns I have a current HP of "12"
    # 11 + 1 (effect) = 12 (effect completed)
    And after "1" turns I have a current HP of "12"
    And after "1" turns I have a current HP of "13"
    # 12 + 1 (third round healing) = 13

## Ale

Scenario: Ale effect gives the desired output - same player, beginning
    Given I use the "minimal" database as-is
    And I add player and character "TestAle01"
    And I create an "Ale" potion of amount "0" and duration "2"
    And I give the item a "drinkDesc" of "BURP!"
    And I put the item in the character's left hand
    And I start the server and play
    When I open and drink "bottle"
    And I saw a message "BURP!"

Scenario: Ale effect gives the desired output - same player, wearing off
    Given I use the "minimal" database as-is
    And I add player and character "TestAle01"
    And I give the character an "ale" effect of "999" for "2" turns
    And the server is started
    When I log on as "TestAle01"
    And I enter the game
    And within "2" turns I see the message "The Ale spell has worn off."

Scenario: Ale effect gives the desired output - other player in same cell
    Given I use the "minimal" database as-is
    And I add player and character "TestAle01"
    And I create an "Ale" potion of amount "0" and duration "2"
    And I give the item a "drinkDesc" of "BURP!"
    And I put the item in the character's left hand
    And I add player and character "TestAle02"
    And the server is started
    When I log on as "TestAle02"
    And I enter the game
    And I rest
    And I cause "TestAle01" to log on and enter the game
    And I cause the secondary player to "open bottle; drink bottle"
    Then within "4" turns I see the message "TestAle01_name burps loudly."

Scenario: Ale effect gives the desired output - other player in neighboring cell
    Given I use the "minimal" database as-is
    And I add player and character "TestAle01"
    And I give the character an "ale" effect of "999" for "999" turns
    And I add player and character "TestAle02"
    And the server is started
    When I log on as "TestAle02"
    And I enter the game
    And I move east
    Then I cause "TestAle01" to log on and enter the game
    And within "1" turns I see the message "TestAle01_name burps loudly."

# bug - Initial effect occurs twice.
@bug
Scenario: Ale effect gives the desired output - burp only once
    Given I use the "minimal" database as-is
    And I add player and character "TestAle01"
    And I give the character an "ale" effect of "999" for "999" turns
    And I add player and character "TestAle02"
    And the server is started
    When I log on as "TestAle02"
    And I enter the game
    And I move east
    Then I cause "TestAle01" to log on and enter the game
    And within "2" turns I see the message "TestAle01_name burps loudly." only once


## Beer

# bug - No indication of the burp is sent to the player themselves.
@bug
Scenario: Beer effect gives the desired output - same player, beginning
    Given I use the "minimal" database as-is
    And I add player and character "TestBeer01"
    And I give the character a "beer" effect of "999" for "2" turns
    And the server is started
    When I log on as "TestBeer01"
    And I enter the game
    And I saw a message "You burp."

Scenario: Beer effect gives the desired output - same player, wearing off
    Given I use the "minimal" database as-is
    And I add player and character "TestBeer01"
    And I give the character a "beer" effect of "999" for "2" turns
    And the server is started
    When I log on as "TestBeer01"
    And I enter the game
    And within "2" turns I see the message "The Beer spell has worn off."

# bug - Burp not indicated to another player in the same cell.
@bug
Scenario: Beer effect gives the desired output - other player in same cell
    Given I use the "minimal" database as-is
    And I add player and character "TestBeer01"
    And I give the character a "beer" effect of "999" for "999" turns
    And I add player and character "TestBeer02"
    And the server is started
    When I log on as "TestBeer02"
    And I enter the game
    And I rest
    Then I cause "TestBeer01" to log on and enter the game
    And within "4" turns I see the message "TestBeer01_name burps."

Scenario: Beer effect gives the desired output - other player in neighboring cell
    Given I use the "minimal" database as-is
    And I add player and character "TestBeer01"
    And I give the character a "beer" effect of "999" for "999" turns
    And I add player and character "TestBeer02"
    And the server is started
    When I log on as "TestBeer02"
    And I enter the game
    And I move east
    Then I cause "TestBeer01" to log on and enter the game
    And within "1" turns I see the message "TestBeer01_name burps."

# bug - Initial effect occurs twice.
@bug
Scenario: Beer effect gives the desired output - burp only once
    Given I use the "minimal" database as-is
    And I add player and character "TestBeer01"
    And I give the character a "beer" effect of "999" for "999" turns
    And I add player and character "TestBeer02"
    And the server is started
    When I log on as "TestBeer02"
    And I enter the game
    And I move east
    Then I cause "TestBeer01" to log on and enter the game
    And within "2" turns I see the message "TestBeer01_name burps." only once

## Blindness_Cure
Scenario: Blindness Cure cures Blindness
    Given I use the "minimal" database as-is
    And I add player and character "TestBlind01"
    And I give the character a "blind" effect of "1" for "999" turns
    And the server is started
    And I log on as "TestBlind01"
    And I enter the game
    And within "1" turns I see the message "You are blind!"
    # we shouldn't have to wait a turn to be blind; bug?
    # And I saw a message "You are blind!"
    And I exit the game
    And I exit the chat and am immediately disconnected
    And I wait for 15 seconds "to make sure the player is fully logged out"
    When I give the character a "blindness_cure" effect of "1" for "1" turns
    And I log on as "TestBlind01"
    And I enter the game
    Then I did not see a message "You are blind!"

# bug - blindness cure output is not shown when the player enters the game
@bug
Scenario: Blindness Cure gives appropriate output on curing blindness while offline
    Given I use the "minimal" database as-is
    And I add player and character "TestBlind02"
    And I give the character a "blind" effect of "1" for "999" turns
    And the server is started
    And I log on as "TestBlind02"
    And I enter the game
    And I saw a message "You are blind!"
    And I exit the game
    And I exit the chat
    And I log out
    When I give the character a "blindness_cure" effect of "1" for "1" turns
    And I log on as "TestBlind02"
    And I enter the game
    Then within "1" turns I see the message "Your blindness has been cured!"

Scenario: Blindness Cure gives appropriate output on curing blindness while online
    Given I use the "minimal" database as-is
    And I add player and character "TestBlind03"
    And I give the character a "blind" effect of "1" for "999" turns
    And I put a blindness cure in the character's left hand
    And I start the server and play
    And I enter the game
    And I saw a message "You are blind!"
    When I open and drink "bottle"
    Then within "1" turns I see the message "Your blindness has been cured!"

Scenario: Blindness Cure gives no output if not blind
    Given I use the "minimal" database as-is
    And I add player and character "TestBlind04"
    When I give the character a "blindness_cure" effect of "1" for "1" turns
    And the server is started
    And I log on as "TestBlind04"
    And I enter the game
    Then within "2" turns I never see the message "Your blindness has been cured!"

Scenario: Blindness Cure gives appropriate output when wearing off
    Given I use the "minimal" database as-is
    And I add player and character "TestBlind05"
    And I give the character a "blind" effect of "1" for "999" turns
    And the server is started
    And I log on as "TestBlind05"
    And I enter the game
    And within "1" turns I see the message "You are blind!"
    # we shouldn't have to wait a turn to be blind; bug?
    # And I saw a message "You are blind!"
    And I exit the game
    And I exit the chat and am immediately disconnected
    And I wait for 15 seconds "to make sure the player is fully logged out"
    When I give the character a "blindness_cure" effect of "1" for "1" turns
    And I log on as "TestBlind05"
    And I enter the game
    Then within "1" turns I see the message "The Cure Blindness spell has worn off."

## Coffee

Scenario: Coffee increases stamina
    Given I use the "minimal" database as-is
    And I add player and character "TestCoffee01"
    And I set the character's max stamina to "10"
    And I set the character's current stamina to "1"
    And I put a coffee in the character's left hand
    And the server is started
    And I log on as "TestCoffee01"
    And I enter the game
    When I open "bottle"
    And I drink "bottle"
    Then after "1" turns I have a current stamina of "3"

Scenario: Coffee increases stamina past max
    Given I use the "minimal" database as-is
    And I add player and character "TestCoffee02"
    And I set the character's max stamina to "10"
    And I set the character's current stamina to "10"
    And I put a coffee in the character's left hand
    And the server is started
    And I log on as "TestCoffee02"
    And I enter the game
    When I open "bottle"
    And I drink "bottle"
    Then after "1" turns I have a current stamina of "11"

Scenario: Coffee does not increase max stamina
    Given I use the "minimal" database as-is
    And I add player and character "TestCoffee03"
    And I set the character's "strength" stat to "3"
    And I set the character's max stamina to "10"
    And I set the character's current stamina to "10"
    And I put a coffee in the character's left hand
    And I put a stone halberd in the character's right hand
    And the server is started
    And I log on as "TestCoffee03"
    And I enter the game
    When I open "bottle"
    And I drink "bottle"
    And I have a current stamina of "11"
    And I move east
    And I have a current stamina of "10"
    And I rest
    Then I have a current stamina of "10"
