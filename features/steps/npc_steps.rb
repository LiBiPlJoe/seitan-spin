# frozen_string_literal: true

When(/^I add an immediate NPC spawn of a wolf on the spawn point$/) do
  npc_id = lookup_npc_id({ name: 'wolf' })
  to_spawn = { enabled: 1, npcID: npc_id, spawnTimer: 1 }
  @zone_id = db_insert_spawn_zone(to_spawn)
end

And(/^I remove the spawn$/) do
  db_remove_spawn_zone(@zone_id)
end
