# frozen_string_literal: true

Given(/^I ensure a developer account exists$/) do
  @user = { acct_name: 'dev01', acct_password: 'devpass01' }
  ensure_developer_account_exists(@user[:acct_name], @user[:acct_password])
end

Given(/^I ensure a standard account exists$/) do
  @user = { acct_name: 'user01', acct_password: 'userpass01' }
  ensure_standard_account_exists(@user[:acct_name], @user[:acct_password])
end

Given(/^I add player and character "([^"]*)"$/) do |acct_name|
  @user = { acct_name: acct_name, acct_password: 'password' }
  ensure_standard_account_exists(@user[:acct_name], @user[:acct_password])
end

Given(/^I make the character a thief$/) do
  db_make_player_thief(@user[:char_name])
end
