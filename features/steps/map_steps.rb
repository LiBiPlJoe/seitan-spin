# frozen_string_literal: true

Given(/^I remove the karma res point for map "([^"]*)"$/) do |map_id|
  move_karma_res_point(map_id.to_i, -1, -1, -1)
end

Given(/^I disable the spawn zones$/) do
  disable_spawn_zones
end
