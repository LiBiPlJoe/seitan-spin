# frozen_string_literal: true

And(/^I set "([^"]*)" in the config file to "([^"]*)"$/) do |config_key, config_value|
  set_value_in_configfile(config_key, config_value)
end
