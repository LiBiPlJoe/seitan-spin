# frozen_string_literal: true

Then(/^the log shows the application version as "([^"]*)"$/) do |app_version|
  log_contains("% #{app_version}", 'SystemGo')
end

Then(/^the log shows the application name as "([^"]*)"$/) do |app_name|
  log_contains("#{app_name} %", 'SystemGo')
end

Then(/^the log shows (a|the) fatal error "([^"]*)"$/) do |_athe, error_text|
  log_contains(error_text.to_s, 'SystemFatalError')
end
