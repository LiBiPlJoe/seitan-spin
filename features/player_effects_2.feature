@kill_server_after
Feature: PlayerEffects

## Drake_Potion

Scenario: Drake potion increases constitution
    Given I use the "minimal" database as-is
    And I add player and character "TestDP01"
    And I set the character's "constitution" stat to "10"
    And I put a drake potion in the character's left hand
    And the server is started
    And I log on as "TestDP01"
    And I enter the game
    When I open "bottle"
    And I drink "bottle"
    Then I have a constitution stat of "11"

Scenario: Drake potion doesn't increase constitution beyond Land max
    Given I use the "minimal" database as-is
    And I add player and character "TestDP02"
    And I set the character's "constitution" stat to "18"
    And I put a drake potion in the character's left hand
    And the server is started
    And I log on as "TestDP02"
    And I enter the game
    When I open "bottle"
    And I drink "bottle"
    Then I have a constitution stat of "18"

Scenario: Drake potion increases max HP and heals fully
    Given I use the "minimal" database as-is
    And I add player and character "TestDP03"
    And I set the character's max HP to "10"
    And I set the character's current HP to "5"
    And I put a drake potion in the character's left hand
    And the server is started
    And I log on as "TestDP03"
    And I enter the game
    When I open "bottle"
    And I drink "bottle"
    Then I have a current and max HP of "14"

Scenario: Drake potion increases max stamina and rests fully
    Given I use the "minimal" database as-is
    And I add player and character "TestDP04"
    And I set the character's max stamina to "10"
    And I set the character's current stamina to "5"
    And I put a drake potion in the character's left hand
    And I start the server and play
    When I open "bottle"
    And I drink "bottle"
    Then I have a current stamina of "14"
    And I rest
    And I have a current stamina of "14"

Scenario: Drake potion restores mana fully
    Given I use the "minimal" database as-is
    And I add player and character "TestDP05"
    And I make the character a thief
    And I set the character's max mana to "10"
    And I set the character's current mana to "5"
    And I put a drake potion in the character's left hand
    And I start the server and play
    And I wait for "1" turns
    And I have a current mana of "5"
    When I open "bottle"
    And I drink "bottle"
    Then I have a current mana of "10"

## Mana_Restore

Scenario: Mana restore potion restores mana fully
    Given I use the "minimal" database as-is
    And I add player and character "TestMR01"
    And I make the character a thief
    And I set the character's max mana to "10"
    And I set the character's current mana to "5"
    And I put a "Mana_Restore" potion in the character's left hand
    And I start the server and play
    And I wait for "1" turns
    And I have a current mana of "5"
    When I open and drink "bottle"
    Then I have a current mana of "10"

## Naphtha

Scenario: Naphtha is fatally poisonous at 50 HP
    Given I use the "minimal" database as-is
    And I add player and character "TestNaph01"
    And I set the character's current and max HP to "50"
    And I put a "Naphtha" potion in the character's left hand
    And I start the server and play
    When I open and drink "bottle"
    Then after "2" turns I have a current HP of "35"
    And after "6" turns I have a current HP of "10"
    And within "3" turns I see the message "You have died from poison."

@slow
Scenario: Naphtha is severely poisonous at 80 HP
    Given I use the "minimal" database as-is
    And I add player and character "TestNaph02"
    And I set the character's current and max HP to "80"
    And I put a "Naphtha" potion in the character's left hand
    And I start the server and play
    When I open and drink "bottle"
    Then after "2" turns I have a current HP of "65"
    And after "3" turns I have a current HP of "52"
    And after "3" turns I have a current HP of "40"
    And after "3" turns I have a current HP of "29"
    And after "3" turns I have a current HP of "19"
    And after "3" turns I have a current HP of "10"
    And after "3" turns I have a current HP of "2"
    And within "2" turns I see the message "The Poison spell has worn off."
    And after "3" turns I have a current HP of "3"

## Nitro - marked TODO in server code

## OrcBalm ("Orc Urine")

Scenario: Drinking orc urine is stunning
    Given I use the "minimal" database as-is
    And I add player and character "TestOrcB01"
    And I put an "OrcBalm" potion in the character's left hand
    And I start the server and play
    When I open and drink "bottle"
    Then I saw the message "You are stunned!"
    And within "6" turns I no longer see the message "You are stunned!"

## Permanent_Mana

# TODO: Tests for effects for which there is not an item in the catalog.

## Stamina_Restore

Scenario: Stamina restore potion rests fully
    Given I use the "minimal" database as-is
    And I add player and character "TestSR01"
    And I set the character's max stamina to "10"
    And I set the character's current stamina to "0"
    And I put a "Stamina_Restore" potion in the character's left hand
    And I start the server and play
    And I have a current stamina of "0"
    When I open and drink "bottle"
    Then I have a current stamina of "10"
    And I rest
    And I have a current stamina of "10"

## Water - has no effect
# TODO: test "no effect"

## Wine - has no effect (I call this a bug.)
# TODO: implement wine effect on server and test it here

## Youth_Potion

Scenario: Youth potion restores youth
    Given I use the "minimal" database as-is
    And I add player and character "TestYP01"
    And I set the character's current age to "75000"
    And I put a "Youth_Potion" potion in the character's left hand
    And I start the server and play
    And I have an age of "ancient"
    When I open and drink "bottle"
    Then I saw the message "You feel young again!"
    And I have an age of "young"

Scenario: Youth potion disappoints the young
    Given I use the "minimal" database as-is
    And I add player and character "TestYP02"
    And I set the character's current age to "14000"
    And I put a "Youth_Potion" potion in the character's left hand
    And I start the server and play
    And I have an age of "very young"
    When I open and drink "bottle"
    Then I saw the message "The fluid is extremely bitter."
    And I have an age of "very young"

## HitsMax
## Hits
## Stamina

# TODO: Tests for effects for which there is not an item in the catalog.

## Permanent_Strength

Scenario: Permanent strength increases strength permanently
    Given I use the "minimal" database as-is
    And I add player and character "TestPS01"
    And I set the character's "strength" stat to "10"
    And I put a "Permanent_Strength" potion in the character's left hand
    And I start the server and play
    And I have a "strength" stat of "10"
    When I open and drink "bottle"
    Then I saw the message "You are stunned!"
    And within "3" turns I no longer see the message "You are stunned!"
    And I have a "strength" stat of "11"

Scenario: Permanent strength does not increase beyond Land max
    Given I use the "minimal" database as-is
    And I add player and character "TestPS02"
    And I set the character's "strength" stat to "18"
    And I put a "Permanent_Strength" potion in the character's left hand
    And I start the server and play
    And I have a "strength" stat of "18"
    When I open and drink "bottle"
    Then I saw the message "You are stunned!"
    And within "3" turns I no longer see the message "You are stunned!"
    And I have a "strength" stat of "18"

## Permanent_Dexterity

Scenario: Permanent dexterity increases dexterity permanently
    Given I use the "minimal" database as-is
    And I add player and character "TestPD01"
    And I set the character's "dexterity" stat to "10"
    And I put a "Permanent_Dexterity" potion in the character's left hand
    And I start the server and play
    And I have a "dexterity" stat of "10"
    When I open and drink "bottle"
    Then I saw the message "You are blind!"
    And within "3" turns I no longer see the message "You are blind!"
    And I have a "dexterity" stat of "11"

Scenario: Permanent dexterity does not increase beyond Land max
    Given I use the "minimal" database as-is
    And I add player and character "TestPD02"
    And I set the character's "dexterity" stat to "18"
    And I put a "Permanent_Dexterity" potion in the character's left hand
    And I start the server and play
    And I have a "dexterity" stat of "18"
    When I open and drink "bottle"
    Then I saw the message "You are blind!"
    And within "3" turns I no longer see the message "You are blind!"
    And I have a "dexterity" stat of "18"

## Permanent_Intelligence, Permanent_Wisdom, Permanent_Constitution, Permanent_Charisma

Scenario Outline: Permanent stat potion increases stat permanently
    Given I use the "minimal" database as-is
    And I add player and character "TestPS01"
    And I set the character's "<stat>" stat to "<init>"
    And I create an "<effect>" potion of amount "<amount>" and duration "0"
    And I put the item in the character's left hand
    And I start the server and play
    And I have a "<stat>" stat of "<init>"
    When I open and drink "bottle"
    And I have a "<stat>" stat of "<expect>"
Examples:
| stat         | effect                 | init | amount | expect |
| intelligence | Permanent_Intelligence | 10   | 1      | 11     |
| intelligence | Permanent_Intelligence | 18   | 2      | 18     |
| constitution | Permanent_Constitution | 10   | 3      | 13     |
| constitution | Permanent_Constitution | 18   | 2      | 18     |
| charisma     | Permanent_Charisma     | 10   | 1      | 11     |
| charisma     | Permanent_Charisma     | 18   | 2      | 18     |
| wisdom       | Permanent_Wisdom       | 10   | 3      | 13     |
| wisdom       | Permanent_Wisdom       | 18   | 2      | 18     |

## Temporary_Strength, Temporary_Dexterity, Temporary_Intelligence, Temporary_Wisdom,
##   Temporary_Constitution, Temporary_Charisma
# Note: We're also verifying that we get temporary boosts even if we're at the max for the Land.

Scenario Outline: Temporary stat potion increases stat temporarily
    Given I use the "minimal" database as-is
    And I add player and character "TestTS01"
    And I set the character's "<stat>" stat to "<init>"
    And I create an "<effect>" potion of amount "<amount>" and duration "2"
    And I put the item in the character's left hand
    And I start the server and play
    And I have a "<stat>" stat of "<init>"
    When I open and drink "bottle"
    And I have a temporary "<stat>" stat of "<amount>"
    And I rest
    And I have a temporary "<stat>" stat of "0"
Examples:
| stat         | effect                 | init | amount |
| strength     | Temporary_Strength     | 10   | 1      |
| strength     | Temporary_Strength     | 18   | 2      |
| dexterity    | Temporary_Dexterity    | 10   | 3      |
| dexterity    | Temporary_Dexterity    | 18   | 2      |
| intelligence | Temporary_Intelligence | 10   | 1      |
| intelligence | Temporary_Intelligence | 18   | 2      |
| constitution | Temporary_Constitution | 10   | 3      |
| constitution | Temporary_Constitution | 18   | 2      |
| charisma     | Temporary_Charisma     | 10   | 1      |
| charisma     | Temporary_Charisma     | 18   | 2      |
| wisdom       | Temporary_Wisdom       | 10   | 3      |
| wisdom       | Temporary_Wisdom       | 18   | 2      |

# TODO: Stack temporary effects

## Shield

# Notes:
# Temporary character attribute "Shielding"; there is a Land.MaxShielding.
# Land max is 9 in the current db.
# Visible via command 'show effects' as "Total Shielding = <value>".
# Value is subtracted from armor class for ranged attacks, half for melee.

Scenario: Temporary shield potion increases shield temporarily
    Given I use the "minimal" database as-is
    And I add player and character "TestTS02"
    And I create a "shield" potion of amount "5" and duration "2"
    And I put the item in the character's left hand
    And I start the server and play
    And I have a shielding of "0"
    When I open and drink "bottle"
    Then I have a shielding of "5"
    And I rest
    And I have a shielding of "0"

Scenario: Temporary shield potion maxes out at Land max
    Given I use the "minimal" database as-is
    And I add player and character "TestTS03"
    And I create a "shield" potion of amount "10" and duration "2"
    And I put the item in the character's left hand
    And I start the server and play
    And I have a shielding of "0"
    When I open and drink "bottle"
    Then I have a shielding of "9"
    And I rest
    And I have a shielding of "0"

# bug - rather than having the amount updated, we wind up with 0?
@bug
Scenario: Multiple temporary shield potions
    Given I use the "minimal" database as-is
    And I add player and character "TestTS04"
    And I create a "shield" potion of amount "6" and duration "99"
    And I put the item in the character's right hand
    And I create a "shield" potion of amount "9" and duration "99"
    And I put the item in the character's left hand
    And I start the server and play
    And I have a shielding of "0"
    When I open and drink "bottle"
    Then I have a shielding of "6"
    And I drop "right"
    And I open and drink "bottle"
    And I have a shielding of "9"

## Resistance and Protection effects
# TODO - verify no unintended resistance/protection
Scenario Outline: Resistance and protection potions have desired effects
    Given I use the "minimal" database as-is
    And I add player and character "TestRP01"
    And I create an "<effect>" potion of amount "<amt>" and duration "3"
    And I put the item in the character's left hand
    And I start the server and play
    And I have a "<resist>" resistance of "0"
    And I have a "<protect>" protection of "0"
    When I open and drink "bottle"
    And I have a "<resist>" resistance of "<resistamt>"
    And I have a "<protect>" protection of "<protectamt>"
    And I rest
    And I have a "<resist>" resistance of "0"
    And I have a "<protect>" protection of "0"
Examples:
| effect                         | amt | resist    | resistamt | protect | protectamt |
| Protection_from_Fire           | 2   | Fire      | 1         | Fire    | 2          |
| Protection_from_Cold           | 3   | Ice       | 1         | Ice     | 3          |
| Protection_from_Fire_and_Ice   | 4   | Fire      | 1         | Fire    | 4          |
| Protection_from_Fire_and_Ice   | 5   | Ice       | 1         | Ice     | 5          |
| Protection_from_Poison         | 6   | Poison    | 1         | Poison  | 6          |
| Protection_from_Stun_and_Death | 8   | Death     | 1         | Death   | 8          |
| Protection_from_Stun_and_Death | 9   | Stun      | 1 | | |
| Protection_from_Blind_and_Fear | 7   | Blind     | 1 | | |
| Protection_from_Blind_and_Fear | 8   | Fear      | 1 | | |
| Resist_Fear                    | 7   | Fear      | 1 | | |
| Resist_Blind                   | 0   | Blind     | 1 | | |
| Resist_Stun                    | 5   | Stun      | 1 | | |
| Resist_Lightning               | 0   | Lightning | 1 | | |

# bug? - Resist_Death adds death resistance but removes death protection when it wears off
# NOTE: Merge with other test when fixed.
@bug
Scenario Outline: Death resistance potion has desired effect
    Given I use the "minimal" database as-is
    And I add player and character "TestRP02"
    And I create an "<effect>" potion of amount "<amt>" and duration "3"
    And I put the item in the character's left hand
    And I start the server and play
    And I have a "<resist>" resistance of "0"
    And I have a "<protect>" protection of "0"
    When I open and drink "bottle"
    And I have a "<resist>" resistance of "<resistamt>"
    And I have a "<protect>" protection of "<protectamt>"
    And I rest
    And I have a "<resist>" resistance of "0"
    And I have a "<protect>" protection of "0"
Examples:
| effect                         | amt | resist    | resistamt | protect | protectamt |
| Resist_Death                   | 3   | Death     | 1 | | |

## Bless

Scenario: Bless potion has desired effect
    Given I use the "minimal" database as-is
    And I add player and character "TestBP01"
    And I set the character's "dexterity" stat to "10"
    And I set the character's "constitution" stat to "8"
    And I create an "Bless" potion of amount "0" and duration "6"
    And I put the item in the character's left hand
    And I start the server and play
    When I open and drink "bottle"
    And I have a "dexterity" stat of "10"
    And I have a temporary "dexterity" stat of "1"
    And I have a "constitution" stat of "8"
    And I have a temporary "constitution" stat of "1"
    And I have a shielding of "1"
    And I rest
    And I have a "dexterity" stat of "10"
    And I have a temporary "dexterity" stat of "0"
    And I have a "constitution" stat of "8"
    And I have a temporary "constitution" stat of "0"
    And I have a shielding of "0"

Scenario: Multiple bless potions have desired effect (i.e. they don't stack)
    Given I use the "minimal" database as-is
    And I add player and character "TestBP02"
    And I set the character's "dexterity" stat to "10"
    And I set the character's "constitution" stat to "8"
    And I create an "Bless" potion of amount "99" and duration "6"
    And I put the item in the character's left hand
    And I put the item in the character's right hand
    And I start the server and play
    When I open and drink "bottle"
    And I drop "right"
    And I open and drink "bottle"
    And I have a "dexterity" stat of "10"
    And I have a temporary "dexterity" stat of "1"
    And I have a "constitution" stat of "8"
    And I have a temporary "constitution" stat of "1"
    And I have a shielding of "1"
    And I rest
    And I have a "dexterity" stat of "10"
    And I have a temporary "dexterity" stat of "0"
    And I have a "constitution" stat of "8"
    And I have a temporary "constitution" stat of "0"
    And I have a shielding of "0"

## Hide_In_Shadows
# Appears to have no effect

## Peek
# Requires a caster, not just a target

## Wizard_Eye
Scenario: Wizard Eye potion results in OOBE
    Given I use the "minimal" database as-is
    And I add player and character "TestHS01"
    And I create an "Wizard_Eye" potion of amount "0" and duration "2"
    And I put the item in the character's left hand
    And I start the server and play
    When I open and drink "bottle"
    Then I saw myself
    And I see my doppelganger in a trance
    And I rest
    And I did not see myself

Scenario: Wizard Eye potion effect visible by others
    Given I use the "minimal" database as-is
    And I add player and character "TestHS01"
    And I create a "Wizard_Eye" potion of amount "0" and duration "50"
    And I put the item in the character's left hand
    And I start the server and play
    When I open and drink "bottle"
    And I add player and character "TestHS02"
    And I log on as "TestHS02"
    And I enter the game
    Then I saw a "toad"
    And I see "TestHS01_name" in a trance
