@kill_server_after
Feature: Server Config file

## APP_VERSION

Scenario: APP_VERSION reflected in the log
	Given I use the "minimal" database
	And I set "APP_VERSION" in the config file to "12.34.567"
	When the server executable is started
	And I allow time for the server to complete startup
	Then the log shows the application version as "12.34.567"

Scenario: APP_VERSION reflected in the welcome message
	Given I use the "minimal" database as-is
	And I set "APP_VERSION" in the config file to "34.56.789"
	And the server executable is started
	And I allow time for the server to complete startup
	When a telnet connection
	Then I see a welcome message with version "34.56.789"

Scenario: APP_VERSION reflected in the main menu
	Given I use the "minimal" database as-is
	And I set "APP_VERSION" in the config file to "45.56.789"
	And the server executable is started
	And I allow time for the server to complete startup
	When I log on using a standard account
	Then I saw a main menu with version "45.56.789"

Scenario: APP_VERSION reflected in the account menu
	Given I use the "minimal" database as-is
	And I set "APP_VERSION" in the config file to "56.56.789"
	And the server executable is started
	And I allow time for the server to complete startup
	When I log on using a standard account
	And I go to the account menu from the main menu
	Then I saw an account menu with version "56.56.789"

Scenario: APP_VERSION reflected in the character menu
	Given I use the "minimal" database as-is
	And I set "APP_VERSION" in the config file to "66.56.789"
	And the server executable is started
	And I allow time for the server to complete startup
	When I log on using a standard account
	And I go to the character menu from the main menu
	Then I saw a character menu with version "66.56.789"

## CLIENT_VERSION

Scenario: CLIENT_VERSION sent at main menu
	Given I use the "minimal" database as-is
	And I set "CLIENT_VERSION" in the config file to "25.6.2.4"
	And the server is started
	When I log on using a standard account
	And I send the protocol command "SET_PROTOCOL"
	Then I receive a CLIENT_VERSION of "25.6.2.4"

## APP_NAME

Scenario: APP_NAME reflected in the log
	Given I use the "minimal" database
	And I set "APP_NAME" in the config file to "Darg Snip Not"
	When the server executable is started
	And I allow time for the server to complete startup
	Then the log shows the application name as "Darg Snip Not"

Scenario: APP_NAME reflected in the welcome message
	Given I use the "minimal" database as-is
	And I set "APP_NAME" in the config file to "Drag Snip Not"
	And the server executable is started
	And I allow time for the server to complete startup
	When a telnet connection
	Then I see a welcome message with name "Drag Snip Not"

Scenario: APP_NAME used when trying to ignore a staff member
	Given I use the "minimal" database
	And I set "APP_NAME" in the config file to "Test Ignore Staff"
	And I ensure a developer account exists
	And the server executable is started
	And I allow time for the server to complete startup
	When I log on using a standard account
	And I enter the chat
	And I try to ignore the developer account
	Then I saw an ignore rejection with application name "Test Ignore Staff"

Scenario: APP_NAME used in user list
	Given I use the "minimal" database as-is
	And I set "APP_NAME" in the config file to "Test UseLis"
	And the server executable is started
	And I allow time for the server to complete startup
	When I log on using a standard account
	And I enter the chat
	And I issue the userlist command
	Then I saw a user list with application name "Test UseLis"

Scenario: APP_NAME used in conference help
	Given I use the "minimal" database as-is
	And I set "APP_NAME" in the config file to "Test ConHel"
	And the server executable is started
	And I allow time for the server to complete startup
	When I log on using a standard account
	And I enter the chat
	And I issue the help command
	Then I saw conference help with application name "Test ConHel"

Scenario: APP_NAME reflected in the main menu
	Given I use the "minimal" database as-is
	And I set "APP_NAME" in the config file to "Test MaiMen"
	And the server executable is started
	And I allow time for the server to complete startup
	When I log on using a standard account
	Then I saw a main menu with application name "Test MaiMen"

Scenario: APP_NAME reflected in the account menu
	Given I use the "minimal" database as-is
	And I set "APP_NAME" in the config file to "Test AccMen"
	And the server executable is started
	And I allow time for the server to complete startup
	When I log on using a standard account
	And I go to the account menu from the main menu
	Then I saw an account menu with application name "Test AccMen"

Scenario: APP_NAME reflected in the character menu
	Given I use the "minimal" database as-is
	And I set "APP_NAME" in the config file to "Test ChaMen"
	And the server executable is started
	And I allow time for the server to complete startup
	When I log on using a standard account
	And I go to the character menu from the main menu
	Then I saw a character menu with application name "Test ChaMen"

Scenario: APP_NAME reflected in welcome back message
	Given I use the "minimal" database as-is
	And I set "APP_NAME" in the config file to "Test WelBac"
	And the server executable is started
	And I allow time for the server to complete startup
	When I log on using a standard account
	And I log out
	And I log on using a standard account
	Then I saw a welcome back with application name "Test WelBac"

Scenario: APP_NAME used in high score list
	Given I use the "production" database as-is
	And I set "APP_NAME" in the config file to "Test HigSco"
	And the server executable is started
	And I allow time for the server to complete startup
	When I log on using a standard account
	And I enter the chat
	And I issue the high scores command
	Then I saw a high score list with application name "Test HigSco"

## NEWS

Scenario: NEWS is printed when connecting - single line
	Given I use the "minimal" database as-is
	And I set "NEWS" in the config file to "This is a test, do not be alarmed."
	And the server executable is started
	And I allow time for the server to complete startup
	When a telnet connection
	Then I see a news message of "This is a test, do not be alarmed."

Scenario: NEWS is printed when connecting - multiple lines
	Given I use the "minimal" database as-is
	And I set "NEWS" in the config file to "This is a test.^Do not be...^alarmed."
	And the server executable is started
	And I allow time for the server to complete startup
	When a telnet connection
	Then I see a news message of "This is a test\.\nDo not be\.\.\.\nalarmed."

Scenario: NEWS included in WorldInformation
	Given I use the "minimal" database as-is
	And I set "NEWS" in the config file to "This is a test, do not be alarmed."
	And the server is started
	When I log on using a standard account
	And I send the protocol command "SET_PROTOCOL"
	Then I receive a NEWS of "This is a test, do not be alarmed."

## APP_PROTOCOL

Scenario: APP_PROTOCOL set to invalid value - normal
	Given I use the "minimal" database as-is
	And I set "APP_PROTOCOL" in the config file to "normal"
	When the server executable is started
	Then the log shows a fatal error "Invalid APP_PROTOCOL."

Scenario: APP_PROTOCOL set to invalid value - old-kesmai
	Given I use the "minimal" database as-is
	And I set "APP_PROTOCOL" in the config file to "old-kesmai"
	When the server executable is started
	Then the log shows a fatal error "Invalid APP_PROTOCOL."

## ClearStores and RestockStores

Scenario: ClearStores True, RestockStores True causes stores to be reset on server startup
	Given I use the "minimal" database
	And I add a vendor "TestVend01"
	And I give vendor "TestVend01" the following items:
      | itemID  | notes      | original | stocked | restock |
      | 30480   | Small Rock | false    | 1       | 0       |
      | 33020   | Red Berries| true     | 7       | 33      |
	And I set "ClearStores" in the config file to "True"
    And I set "RestockStores" in the config file to "True"
	When the server executable is started
	And I allow time for the server to complete startup
	Then the log shows "1" items cleared from stores

Scenario: ClearStores True, RestockStores True causes stores to be restocked on server startup
	Given I use the "minimal" database
	And I add a vendor "TestVend01"
	And I give vendor "TestVend01" the following items:
      | itemID  | notes      | original | stocked | restock |
      | 30480   | Small Rock | false    | 1       | 0       |
      | 33020   | Red Berries| true     | 7       | 33      |
	And I set "ClearStores" in the config file to "True"
    And I set "RestockStores" in the config file to "True"
	When the server executable is started
	And I allow time for the server to complete startup
	And the log shows "1" items restocked in stores

Scenario: ClearStores True, RestockStores False causes stores to be cleared but not restocked on server startup
	Given I use the "minimal" database
	And I add a vendor "TestVend01"
	And I give vendor "TestVend01" the following items:
      | itemID  | notes      | original | stocked | restock |
      | 30480   | Small Rock | false    | 1       | 0       |
      | 33020   | Red Berries| true     | 7       | 33      |
	And I set "ClearStores" in the config file to "True"
    And I set "RestockStores" in the config file to "False"
	When the server executable is started
	And I allow time for the server to complete startup
	Then the log shows "1" items cleared from stores
	And the log does not show any items restocked in stores

Scenario: ClearStores False, RestockStores True causes stores to be restocked but not cleared on server startup
	Given I use the "minimal" database
	And I add a vendor "TestVend01"
	And I give vendor "TestVend01" the following items:
      | itemID  | notes      | original | stocked | restock |
      | 30480   | Small Rock | false    | 1       | 0       |
      | 33020   | Red Berries| true     | 7       | 33      |
	And I set "ClearStores" in the config file to "False"
    And I set "RestockStores" in the config file to "True"
	When the server executable is started
	And I allow time for the server to complete startup
	Then the log does not show any items cleared from stores
	And the log shows "1" items restocked in stores

Scenario: ClearStores False, RestockStores False causes stores to neither be cleared nor restocked on server startup
	Given I use the "minimal" database
	And I add a vendor "TestVend01"
	And I give vendor "TestVend01" the following items:
      | itemID  | notes      | original | stocked | restock |
      | 30480   | Small Rock | false    | 1       | 0       |
      | 33020   | Red Berries| true     | 7       | 33      |
	And I set "ClearStores" in the config file to "False"
    And I set "RestockStores" in the config file to "False"
	When the server executable is started
	And I allow time for the server to complete startup
	Then the log does not show any items cleared from stores
	And the log does not show any items restocked in stores

## ColorLog appears to only affect the Windows Forms stuff that we're not dealing with at all.

## RequireMakeRecallReagent

Scenario: RequireMakeRecallReagent False, thief can make recall ring from nothing
	Given I use the "minimal" database as-is
	And I set "RequireMakeRecallReagent" in the config file to "False"
	And I add player and character "TestThief01"
	And I make the character a thief
	And I give the character the spell "makerecall"
	And I put a spellbook in the character's right hand
	And the server is started
	When I log on as "TestThief01"
	And I enter the game
	And I read the book
	And I warm the spell "Make Recall"
	And I cast the warmed spell
	Then I have a recall ring in my left hand

Scenario: RequireMakeRecallReagent True, thief cannot make recall ring from nothing
	Given I use the "minimal" database as-is
	And I set "RequireMakeRecallReagent" in the config file to "True"
	And I add player and character "TestThief01"
	And I make the character a thief
	And I give the character the spell "makerecall"
	And I put a spellbook in the character's right hand
	And the server is started
	When I log on as "TestThief01"
	And I enter the game
	And I read the book
	And I warm the spell "Make Recall"
	And I cast the warmed spell
	Then My spellbook exploded
	And I do not have a recall ring in my "left" hand
	And I do not have a recall ring in my "right" hand

Scenario: RequireMakeRecallReagent False, thief can make recall ring from gold ring
	Given I use the "minimal" database as-is
	And I set "RequireMakeRecallReagent" in the config file to "False"
	And I add player and character "TestThief01"
	And I make the character a thief
	And I give the character the spell "makerecall"
	And I put a spellbook in the character's right hand
	And I put a gold ring in the character's left hand
	And the server is started
	When I log on as "TestThief01"
	And I enter the game
	And I read the book
	And I warm the spell "Make Recall"
	And I cast the warmed spell
	Then I have a recall ring in my left hand

Scenario: RequireMakeRecallReagent True, thief can make recall ring from gold ring
	Given I use the "minimal" database as-is
	And I set "RequireMakeRecallReagent" in the config file to "True"
	And I add player and character "TestThief01"
	And I make the character a thief
	And I give the character the spell "makerecall"
	And I put a spellbook in the character's right hand
	And I put a gold ring in the character's left hand
	And the server is started
	When I log on as "TestThief01"
	And I enter the game
	And I read the book
	And I warm the spell "Make Recall"
	And I cast the warmed spell
	Then I have a recall ring in my left hand

## DisplayCombatDamage

Scenario: DisplayCombatDamage True, I can enable combat damage display
	Given I use the "minimal" database as-is
	And I set "DisplayCombatDamage" in the config file to "True"
	And the server is started
	When I log on using a standard account
	And I enter the chat
	And I issue the display combat damage command
	And I did not see a message telling me combat damage display was disabled

Scenario: DisplayCombatDamage False, I cannot enable combat damage display
	Given I use the "minimal" database as-is
	And I set "DisplayCombatDamage" in the config file to "False"
	And the server is started
	When I log on using a standard account
	And I enter the chat
	And I issue the display combat damage command
	And I saw a message telling me combat damage display was disabled

Scenario: DisplayCombatDamage True, I can enable and see combat damage
	Given I use the "minimal" database as-is
	And I set "DisplayCombatDamage" in the config file to "True"
	And I add player and character "TestCombat01"
	And I set the character's "unarmed" skill to "10000000"
	And I add an immediate NPC spawn of a wolf on the spawn point
	And the server is started
	When I log on as "TestCombat01"
	And I enter the chat
	And I issue the display combat damage command
	And I enter the game from chat
	And I wait for "wolf" to appear
	And I attack the "wolf"
	Then I saw numerical combat damage
	And I remove the spawn

## UnderworldEnabled

# Speaking the Ancestor Start Chant ("ashak ashtug nushi ilani") in the ancestor start cell
#   On map test01 this is 50,27,0
Scenario: UnderworldEnabled False, the Ancestor Start Chant does not take me there
	Given I use the "minimal" database as-is
	And I set "UnderworldEnabled" in the config file to "False"
	And I add player and character "TestAncestor01"
	And I move the player to the Ancestor Start cell
	And the server is started
	When I log on as "TestAncestor01"
	And I enter the game
	And I chant "ashak ashtug nushi ilani"
	Then I saw a message "This feature is currently disabled."

Scenario: UnderworldEnabled True, the Ancestor Start Chant takes me there
	Given I use the "minimal" database as-is
	And I set "UnderworldEnabled" in the config file to "True"
	And I add player and character "TestAncestor01"
	And I move the player to the Ancestor Start cell
	And the server is started
	When I log on as "TestAncestor01"
	And I enter the game
	And I chant "ashak ashtug nushi ilani"
	Then I saw a message "The world dissolves around you."

# Speaking the Underworld Portal Chant ("urruku ya zi xul") in the underworld portal cell
#   On map test01 this is 50, 31, 0
Scenario: UnderworldEnabled False, the Underworld Portal Chant does not take me there
	Given I use the "minimal" database as-is
	And I set "UnderworldEnabled" in the config file to "False"
	And I add player and character "TestUnderportal01"
	And I move the player to the Underworld Portal cell
	And the server is started
	When I log on as "TestUnderportal01"
	And I enter the game
	And I chant "urruku ya zi xul"
	Then I saw a message "This feature is currently disabled."

Scenario: UnderworldEnabled True, the Underworld Portal Chant takes me there
	Given I use the "minimal" database as-is
	And I set "UnderworldEnabled" in the config file to "True"
	And I add player and character "TestUnderportal01"
	And I move the player to the Underworld Portal cell
	And the server is started
	When I log on as "TestUnderportal01"
	And I enter the game
	And I chant "urruku ya zi xul"
	Then I saw a message "The world dissolves around you."

# Your corpse is burned while you're still online and with positive karma
Scenario: UnderworldEnabled True, online karmicly challenged burnt corpse goes there
	Given I use the "minimal" database
	And I set "UnderworldEnabled" in the config file to "True"
	And I add player and character "TestBadCorpse01"
	And I set the character's current karma to "1"
	And I set the character's current HP to "1"
	And I put a naphtha in the character's right hand
	And I put a naphtha in the character's left hand
	And the server is started
	When I log on as "TestBadCorpse01"
	And I enter the game
	And I throw a bottle in my cell
	And I speak "this is fine"
	Then I saw a message "The world dissolves around you."

Scenario: UnderworldEnabled False, online karmicly challenged burnt corpse doesn't go there
	Given I use the "minimal" database
	And I set "UnderworldEnabled" in the config file to "False"
	And I add player and character "TestBadCorpse01"
	And I set the character's current karma to "1"
	And I set the character's current HP to "1"
	And I put a naphtha in the character's right hand
	And I put a naphtha in the character's left hand
	And the server is started
	When I log on as "TestBadCorpse01"
	And I enter the game
	And I throw a bottle in my cell
	And I speak "this is fine"
	Then I did not see a message "The world dissolves around you."

# Your corpse is burned while you're still online and of Evil alignment, and fail a constitution saving throw
Scenario: UnderworldEnabled True, online evil low-constitution burnt corpse goes there
	Given I use the "minimal" database
	And I set "UnderworldEnabled" in the config file to "True"
	And I add player and character "TestBadCorpse02"
	And I set the character's alignment to evil
	And I set the character's current HP to "1"
	And I set the character's current constitution to "1"
	And I put a naphtha in the character's right hand
	And I put a naphtha in the character's left hand
	And I disable the spawn zones
	And the server is started
	When I log on as "TestBadCorpse02"
	And I enter the game
	And I throw a bottle in my cell
	And I speak "this is fine"
	Then I saw a message "The world dissolves around you."

Scenario: UnderworldEnabled False, online evil low-constitution burnt corpse doesn't go there
	Given I use the "minimal" database
	And I set "UnderworldEnabled" in the config file to "False"
	And I add player and character "TestBadCorpse02"
	And I set the character's alignment to evil
	And I set the character's current HP to "1"
	And I set the character's current constitution to "1"
	And I put a naphtha in the character's right hand
	And I put a naphtha in the character's left hand
	And the server is started
	When I log on as "TestBadCorpse02"
	And I enter the game
	And I throw a bottle in my cell
	And I speak "this is fine"
	Then I did not see a message "The world dissolves around you."

# You are older than "ancient" and lose a die roll (roughly 1/20 chance per round)
@slow
Scenario: UnderworldEnabled True, ancient character eventually goes there
	Given I use the "minimal" database
	And I set "UnderworldEnabled" in the config file to "True"
	And I add player and character "TestAncientDead01"
	And I set the character's current age to "80001"
	And the server is started
	When I log on as "TestAncientDead01"
	And I enter the game
	Then within "50" turns I see a message "The world dissolves around you."

@slow
Scenario: UnderworldEnabled False, ancient character never goes there
	Given I use the "minimal" database
	And I set "UnderworldEnabled" in the config file to "False"
	And I add player and character "TestAncientDead01"
	And I set the character's current age to "80001"
	And the server is started
	When I log on as "TestAncientDead01"
	And I enter the game
	Then within "50" turns I never see the message "The world dissolves around you."

# Rest when dead with positive karma, in a map with no karma res point, if not a thief
Scenario: UnderworldEnabled True, positive karma non-thief corpse goes there if no karma res point
	Given I use the "minimal" database
	And I set "UnderworldEnabled" in the config file to "True"
	And I remove the karma res point for map "0"
	And I add player and character "TestBadCorpse03"
	And I set the character's current karma to "1"
	And I set the character's current HP to "1"
	And I put poison berries in the character's right hand
	And I put poison berries in the character's left hand
	And the server is started
	When I log on as "TestBadCorpse03"
	And I enter the game
    And I eat "berries"
	And within "10" turns I see the message "You are dead"
	And I rest
	Then I saw a message "The world dissolves around you."

Scenario: UnderworldEnabled False, positive karma non-thief corpse doesn't go there if no karma res point
	Given I use the "minimal" database
	And I set "UnderworldEnabled" in the config file to "False"
	And I remove the karma res point for map "0"
	And I add player and character "TestBadCorpse03"
	And I set the character's current karma to "1"
	And I set the character's current HP to "1"
	And I put poison berries in the character's right hand
	And I put poison berries in the character's left hand
	And the server is started
	When I log on as "TestBadCorpse03"
	And I enter the game
    And I eat "berries"
	And within "10" turns I see the message "You are dead"
	And I rest
	Then I did not see a message "The world dissolves around you."

# Rest when dead but not lawful, in a map with no karma res point, if not a thief
Scenario: UnderworldEnabled True, neutral non-thief corpse goes there if no karma res point
	Given I use the "minimal" database
	And I set "UnderworldEnabled" in the config file to "True"
	And I remove the karma res point for map "0"
	And I add player and character "TestBadCorpse04"
	And I set the character's alignment to neutral
	And I set the character's current HP to "1"
	And I put poison berries in the character's right hand
	And the server is started
	When I log on as "TestBadCorpse04"
	And I enter the game
    And I eat "berries"
	And within "10" turns I see the message "You are dead"
	And I rest
	Then I saw a message "The world dissolves around you."

Scenario: UnderworldEnabled False, neutral non-thief corpse doesn't go there if no karma res point
	Given I use the "minimal" database
	And I set "UnderworldEnabled" in the config file to "False"
	And I remove the karma res point for map "0"
	And I add player and character "TestBadCorpse04"
	And I set the character's alignment to neutral
	And I set the character's current HP to "1"
	And I put poison berries in the character's right hand
	And the server is started
	When I log on as "TestBadCorpse04"
	And I enter the game
    And I eat "berries"
	And within "10" turns I see the message "You are dead"
	And I rest
	Then I did not see a message "The world dissolves around you."

### NPCSkillGain
# @bug
# bug - NPCs never trigger the skill gain code at all.
# Stealing
# Casting
# Special block in combat
# Combat damage
# Magic spell success
