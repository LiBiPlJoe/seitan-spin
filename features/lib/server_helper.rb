# frozen_string_literal: true

def start_server
  containers_up = verify_containers_up

  # Start up the game server
  stdout, stderr, status = Open3.capture3(
    'docker exec --detach seitan-spin-gameserver-1 "./entrypoint.sh"'
  )
  debug_msg "Result of docker exec: #{status.inspect}"
  debug_msg "  stdout: #{stdout}" if stdout
  debug_msg "  stderr: #{stderr}" if stderr

  containers_up && status.success?
end

def take_containers_down
  # Make sure the containers are down
  stdout, stderr, status = Open3.capture3('docker compose down -v')
  debug_msg "Result of docker compose down: #{status.inspect}"
  debug_msg "  stdout: #{stdout}" if stdout
  debug_msg "  stderr: #{stderr}" if stderr

  # Close DB connection
  if @db_client&.active?
    debug_msg 'Closing db connection after taking containers down'
    @db_client.close
  end

  status.success?
end

def verify_containers_up
  # Make sure the containers are up
  stdout, stderr, status = Open3.capture3('docker compose up --detach')
  debug_msg "Result of docker compose up: #{status.inspect}"
  debug_msg "  stdout: #{stdout}" if stdout
  debug_msg "  stderr: #{stderr}" if stderr

  status.success?
end

# NOTE: Does not check order in log, just waits for second message and then checks
#   log for existance of first message.
def log_not_this_before_that(message1, logtype1, message2, logtype2)
  log_contains(message2, logtype2) && !log_contains_immediate(message1, logtype1)
end
