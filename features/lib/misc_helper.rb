# frozen_string_literal: false

require 'net/telnet'
require 'rspec'

DEFAULT_TELNET = {
  'Host' => ENV.fetch('DS_HOST', nil),
  'Port' => ENV.fetch('DS_PORT', nil),
  'Prompt' => 'Login:',
  'Telnetmode' => false,
  'Timeout' => 20,
  'Waittime' => 0.1,
  'FailEOF' => true
}.freeze

def create_telnet_connection
  rand_id = rand(36**20).to_s(36)
  options = DEFAULT_TELNET.merge('Output_log' => "output_#{rand_id}.log",
                                 'Dump_log' => "dump_#{rand_id}.log")
  debug_msg "Creating telnet connection with ID #{rand_id}"
  begin
    connection = Net::Telnet.new(options)
  rescue Errno::ECONNREFUSED => e
    debug_msg e.inspect
    connection = nil
  end
  connection
end

def random_acct_name(_min_length = 5, max_length = 12, prefix = 'test')
  # public const int ACCOUNT_MIN_LENGTH = 5;
  # public const int ACCOUNT_MAX_LENGTH = 12;
  # string[] silly = new string[]{"pvp","lol","haha","hehe","btw","atm","jeje","rofl","roflmao",
  #  "lmao","lmfao","lmho","dragonsspine","dragonspine","nobody","somebody","anybody","account"};
  # string acceptable = "abcdefghijklmnopqrstuvwxyz0123456789";end
  silly = %w[pvp lol haha hehe btw atm jeje rofl roflmao lmao lmfao lmho dragonsspine
             dragonspine nobody somebody anybody account]
  acceptable = 'ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789'.scan(/./)
  prefix + random_string(max_length - prefix.length, acceptable, silly)
end

def random_acct_password(_min_length = 4, max_length = 12, prefix = '')
  # public const int PASSWORD_MIN_LENGTH = 4;
  # public const int PASSWORD_MAX_LENGTH = 12;
  prefix + random_string(max_length - prefix.length,
                         'ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789'
                         .scan(/./), [])
end

def random_char_name(_min_length = 4, max_length = 14, prefix = 'Dude.')
  # public const short NAME_MIN_LENGTH = 4;
  # public const short NAME_MAX_LENGTH = 14;
  # string[] specialnames = new string[]{"orc","gnoll","goblin","skeleton","kobold","dragon",
  #    "drake","griffin","manticore","banshee","demon","bear","boar","vampire","ydnac","lars",
  #    "sven","oskar","olaf","marlis","neela","phong","ironbar","vulcan","sheriff","rolf","troll",
  #    "wyvern","ydmos","tanner","crazy.derf","trambuskar","ianta","alia","priest","statue",
  #    "shidosha","pazuzu","asmodeus","damballa","glamdrang","samael","perdurabo",
  #    "thamuz","knight","martialartist","thief","thaum","thaumaturge","wizard","fighter",
  #    "thisson"};
  #
  # string[] silly = new string[]{"pvp","lol","haha","hehe","btw","atm","jeje","rofl","roflmao",
  # "lmao","lmfao","lmho","dragonsspine","dragonspine","nobody","somebody","anybody","account",
  # "character"};
  #
  # string acceptable = "abcdefghijklmnopqrstuvwxyz.";
  #
  specialnames = ['orc', 'gnoll', 'goblin', 'skeleton', 'kobold', 'dragon', 'drake', 'griffin',
                  'manticore', 'banshee', 'demon', 'bear', 'boar', 'vampire', 'ydnac', 'lars',
                  'sven', 'oskar', 'olaf', 'marlis', 'neela', 'phong', 'ironbar', 'vulcan',
                  'sheriff', 'rolf', 'troll', 'wyvern', 'ydmos', 'tanner', 'crazy.derf',
                  'trambuskar', 'ianta', 'alia', 'priest', 'statue', 'shidosha', 'pazuzu',
                  'asmodeus', 'damballa', 'glamdrang', 'samael', 'perdurabo', 'thamuz', 'knight',
                  'martialartist', 'thief', 'thaum', 'thaumaturge', 'wizard', 'fighter', 'thisson']
  silly = %w[pvp lol haha hehe btw atm jeje rofl roflmao lmao lmfao lmho dragonsspine
             dragonspine nobody somebody anybody account character]
  acceptable = 'ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz.'.scan(/./)

  # name_length = rand(max = max_length - prefix.length)
  prefix + random_string(max_length - prefix.length, acceptable, specialnames + silly)
end

def random_string(length, acceptable, forbidden)
  ok = false
  until ok
    my_str = ''
    length.times do
      my_str << acceptable.sample
    end
    ok = true
    forbidden.each do |forbidden_str|
      ok = false if my_str.include?(forbidden_str)
    end
  end
  my_str
end

# NOTE: returns output of the last command
def telnet_commands(connection, commands)
  resp = ''
  commands.each do |command|
    @last_resp = resp = telnet_command(connection, command[0], command[1])
  end
  resp
end

def telnet_command(connection, string, match, overrides = {})
  debug_msg "Sending #{string} and waiting for #{match.inspect}..."
  start_time = Time.now
  @last_resp = resp = connection.cmd({ 'String' => string,
                                       'Match' => match }.merge(overrides)) do |c|
    telnet_out(c)
  end
  end_time = Time.now
  elapsed_time = end_time - start_time
  debug_msg "Time waited for telnet response: #{elapsed_time}" if elapsed_time > 1.5
  matches = resp.scan(match)
  if matches && matches.length > 1
    debug_msg "Thank you Waittime, I saw /#{match}/ #{matches.length} times in #{resp}."
  end
  resp
end

# Used only if there is no change of seeing the prompt string before it's actually the prompt.
def telnet_command_fast(connection, string, match)
  debug_msg "Sending #{string} and waiting for #{match.inspect}..."
  start_time = Time.now

  connection.puts(string)
  @last_resp = resp = connection.waitfor({ 'Prompt' => match, 'Waittime' => 0 })
  telnet_out(resp)

  end_time = Time.now
  elapsed_time = end_time - start_time
  debug_msg "Time waited for telnet response: #{elapsed_time}" if elapsed_time > 1.0
  resp
end

def telnet_command_slow(connection, string, match)
  debug_msg "Sending #{string} and waiting for #{match.inspect}..."
  start_time = Time.now

  connection.puts(string)
  @last_resp = resp = connection.waitfor({ 'Prompt' => match, 'Waittime' => 10 })
  telnet_out(resp)

  end_time = Time.now
  elapsed_time = end_time - start_time
  debug_msg "Time waited for telnet response: #{elapsed_time}"
  if len(matches) > 1
    debug_msg "Thank you Waittime, I saw /#{match}/ #{len(matches)} times in #{resp}."
  end
  resp
end

def parse_stats(stats_string)
  # Strength:     18        Adds:     1
  # Dexterity:    16        Adds:     1
  # Intelligence: 18
  # Wisdom:       16        Hits:    33
  # Constitution: 15        Stamina:  6
  # Charisma:     16        Mana:    15
  stats_array = stats_string.scan(/(\w+):\s*(\d+)/)
  stats = {}
  last_stat_name = ''
  stats_array.each do |stat|
    stat_name = stat[0].downcase
    stat_name = last_stat_name + stat_name if stat_name == 'adds'
    stat_value = stat[1].to_i
    stats[stat_name] = stat_value
    last_stat_name = stat_name
  end
  debug_msg "Stats: #{stats}"
  stats
end
