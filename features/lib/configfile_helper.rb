# frozen_string_literal: true

require 'nokogiri'

# Updates a value in the config file with the given key
# Note that the filename and XPath are currently hardcoded.
def set_value_in_configfile(key, value)
  debug_msg "Setting #{key} to #{value} in config file."

  # Open the config file and parse it with Nokogiri
  config_file_path = 'gameserver/DragSpinExp.exe.config'
  config_file = File.open(config_file_path) { |f| Nokogiri::XML(f) }

  # Find the XML element with the given key and update its value
  config_element = config_file.at_xpath("//appSettings//add[@key='#{key}']")
  config_element['value'] = value

  # Write the updated config file back to disk
  File.write(config_file_path, config_file.to_xml)
end
