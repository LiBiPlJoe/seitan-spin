# frozen_string_literal: true

def log_contains(message, logtype = 'SystemGo', log_timeout = 30)
  client = connect_to_db(@server_database)
  rows_affected = 0
  Timeout.timeout(log_timeout) do
    loop do
      debug_msg "Waiting for db #{@server_database} log to contain \"#{message}\" type #{logtype}."
      result = client.execute("SELECT * FROM [#{@server_database}].[dbo].[Log] \
                                WHERE logtype LIKE '#{logtype}' AND message LIKE '#{message}'")
      rows_affected = result.do
      debug_msg "Log select result: #{rows_affected} #{result.inspect}"
      break if rows_affected.positive?

      sleep 1
    end
  end
  rows_affected.positive?
end

def log_contains_immediate(message, logtype = 'SystemGo')
  client = connect_to_db(@server_database)
  debug_msg "Checking log for \"#{message}\"."
  result = client.execute("SELECT * FROM [#{@server_database}].[dbo].[Log] \
                          WHERE logtype LIKE '#{logtype}' AND message LIKE '#{message}'")
  rows_affected = result.do
  rows_affected.positive?
end

def clear_log
  client = connect_to_db(@server_database)
  result = client.execute("DELETE
    FROM [#{@server_database}].[dbo].[Log]")
  debug_msg "Rows deleted from [Log]: #{result.do}"
end
