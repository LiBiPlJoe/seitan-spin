# frozen_string_literal: true

DEFAULT_MAP = {
  mapID: 0,
  landID: 0,
  suggestedMaximumLevel: 99,
  suggestedMinimumLevel: 1,
  pvpEnabled: 0,
  expModifier: 1,
  difficulty: 1,
  climateType: 1,
  balmBushes: 0,
  poisonBushes: 0,
  manaBushes: 0,
  staminaBushes: 0,
  resX: 41,
  resY: 26,
  resZ: 0,
  thiefResX: 42,
  thiefResY: 27,
  thiefResZ: 0,
  karmaResX: -1,
  karmaResY: -1,
  karmaResZ: -1,
  randomMagicIntensity: 0
}.freeze

def move_karma_res_point(map_id, x, y, z)
  client = connect_to_db(@server_database)
  query = "UPDATE [#{@server_database}].[dbo].[Map] SET \
    karmaResX = #{x}, karmaResY = #{y}, karmaResZ = #{z} \
    WHERE mapID = #{map_id}"
  debug_msg query
  result = client.execute(query)
  affected_rows = result.do
  fail 'Unable to update map!' if affected_rows != 1
end

# CREATE TABLE [dbo].[Map](
# 	[mapID] [smallint] NOT NULL,
# 	[landID] [smallint] NOT NULL,
# 	[name] [varchar](50) NULL,
# 	[shortDesc] [varchar](max) NULL,
# 	[longDesc] [varchar](max) NULL,
# 	[suggestedMaximumLevel] [smallint] NOT NULL,
# 	[suggestedMinimumLevel] [smallint] NOT NULL,
# 	[pvpEnabled] [bit] NOT NULL,
# 	[expModifier] [float] NOT NULL,
# 	[difficulty] [smallint] NOT NULL,
# 	[climateType] [smallint] NOT NULL,
# 	[balmBushes] [bit] NOT NULL,
# 	[poisonBushes] [bit] NOT NULL,
# 	[manaBushes] [bit] NOT NULL,
# 	[staminaBushes] [bit] NOT NULL,
# 	[resX] [int] NOT NULL,
# 	[resY] [int] NOT NULL,
# 	[resZ] [int] NOT NULL,
# 	[thiefResX] [int] NOT NULL,
# 	[thiefResY] [int] NOT NULL,
# 	[thiefResZ] [int] NOT NULL,
# 	[karmaResX] [int] NOT NULL,
# 	[karmaResY] [int] NOT NULL,
# 	[karmaResZ] [int] NOT NULL,
# 	[randomMagicIntensity] [bit] NOT NULL
# ) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
# GO
# SET ANSI_PADDING OFF
# GO
# INSERT [dbo].[Map] ([mapID], [landID], [name], [shortDesc], [longDesc], [suggestedMaximumLevel],
#  [suggestedMinimumLevel], [pvpEnabled], [expModifier], [difficulty], [climateType],
#  [balmBushes], [poisonBushes], [manaBushes], [staminaBushes], [resX], [resY], [resZ],
#  [thiefResX], [thiefResY], [thiefResZ], [karmaResX], [karmaResY], [karmaResZ],
#  [randomMagicIntensity])
#   VALUES (0, 0, N'test01', N'test01', NULL, 12, 3, 0, 1, 1, 2, 1, 1, 0, 0, 41, 26, 0, 42, 27, 0,
#   43, 28, 0, 0)
