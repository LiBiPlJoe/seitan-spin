# frozen_string_literal: true

DEFAULT_ITEM = {
  catalogID: 0,
  combatAdds: 0,
  itemID: 0,
  itemType: 'Miscellaneous',
  visualKey: 'unknown',
  wearLocation: 'None',
  weight: 0.0,
  skillType: 'None',
  recall: 0,
  alignment: 'None',
  spell: -1,
  spellPower: -1,
  charges: -1,
  blueglow: 0,
  flammable: 0,
  fragile: 0,
  lightning: 0,
  silver: 0,
  attuneType: 'None',
  armorClass: 0,
  armorType: 0,
  bookType: 'None',
  maxPages: 0,
  pages: '',
  drinkDesc: '',
  fluidDesc: ''
}.freeze

# TODO: refactor "find max" into generic method
def find_next_catalog_id
  client = connect_to_db(@server_database)
  query = 'SELECT MAX(catalogID) AS maxCatID FROM CatalogItem'
  result = client.execute(query)
  row = result.each(first: true, symbolize_keys: true).first
  debug_msg row.inspect
  row[:maxCatID] + 1
end

def find_next_item_id
  client = connect_to_db(@server_database)
  query = 'SELECT MAX(itemID) AS maxID FROM CatalogItem'
  result = client.execute(query)
  row = result.each(first: true, symbolize_keys: true).first
  debug_msg row.inspect
  row[:maxID] + 1
end

def create_item_from_blank(lookup_hash, create_hash)
  client = connect_to_db(@server_database)
  query = db_field_from_hash('CatalogItem', lookup_hash, '*')
  debug_msg "Query is #{query}"
  result = client.execute(query)
  row = result.each(first: true, symbolize_keys: true).first
  debug_msg row.inspect
  new_row = row.merge(create_hash)
  debug_msg new_row
  new_row.delete(:catalogID)
  new_row[:itemID] = find_next_item_id
  query = db_insert_from_hash('CatalogItem', new_row)
  debug_msg query
  result = client.execute(query)
  affected_rows = result.do
  fail 'Unable to insert item!' if affected_rows != 1

  new_row[:itemID]
end

def lookup_item_id(lookup_hash)
  client = connect_to_db(@server_database)
  query = db_field_from_hash('CatalogItem', lookup_hash, 'itemID')
  debug_msg "Query is #{query}"
  result = client.execute(query)
  row = result.each(first: true)
  debug_msg row.inspect
  fail 'Unable to get itemID!' if result.affected_rows < 1

  row[0]['itemID']
end

def lookup_item_special(lookup_hash)
  client = connect_to_db(@server_database)
  query = db_field_from_hash('CatalogItem', lookup_hash, 'special')
  debug_msg "Query is #{query}"
  result = client.execute(query)
  row = result.each(first: true)
  debug_msg row.inspect
  fail 'Unable to get special!' if result.affected_rows != 1

  row[0]['special']
end
