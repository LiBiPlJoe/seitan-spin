# frozen_string_literal: true

EFFECT_TYPE_ENUM = %i[
  none fire ice darkness poison_cloud web light concussion
  find_secret_door illusion dragon__s_breath_fire dragon__s_breath_ice turn_undead
  whirlwind breathe_water feather_fall night_vision wizard_eye peek hitsmax hits
  exp stamina stamleft permanent_strength permanent_dexterity permanent_intelligence
  permanent_wisdom permanent_constitution permanent_charisma temporary_strength
  temporary_dexterity temporary_intelligence temporary_wisdom temporary_constitution
  temporary_charisma shield regenerate_hits regenerate_mana regenerate_stamina
  protection_from_fire protection_from_cold protection_from_poison
  protection_from_fire_and_ice protection_from_blind_and_fear
  protection_from_stun_and_death resist_fear resist_blind resist_stun resist_lightning
  resist_death resist_zonk knight_ring bless blind fear poison balm naphtha
  ale wine beer coffee water youth_potion drake_potion blindness_cure
  mana_restore stamina_restore nitro orcbalm permanent_mana whiskey rum
  unlocked_horizontal_door unlocked_vertical_door hide_in_shadows flight
  find_secret_rockwall hide_door mindcontrol black_fog lightning_storm
  fire_storm lava lightning hello_immobility dog_luck dog_follow
].freeze

ALIGNMENT_ENUM = %i[none lawful neutral chaotic evil amoral all].freeze
