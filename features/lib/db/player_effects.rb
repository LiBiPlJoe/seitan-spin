# frozen_string_literal: true

DEFAULT_PLAYER_EFFECT = {
  PlayerID: 0,
  EffectSlot: 1
}.freeze

def clear_player_effects
  client = connect_to_db(@server_database)
  query = "UPDATE [#{@server_database}].[dbo].[PlayerEffects] SET \
    EffectID = 0, Amount = 0, Duration = 0"
  debug_msg query
  result = client.execute(query)
  affected_rows = result.do
  debug_msg "#{affected_rows} player effects cleared"
end

def add_player_effect(player_id, effect, amount, duration)
  slot = find_empty_effect_slot(player_id)
  client = connect_to_db(@server_database)
  query = "UPDATE [#{@server_database}].[dbo].[PlayerEffects] SET \
    EffectID = #{effect}, Amount = #{amount}, Duration = #{duration} \
    WHERE PlayerID = #{player_id} AND EffectSlot = #{slot}"
  debug_msg query
  result = client.execute(query)
  affected_rows = result.do
  fail 'Unable to update player effect!' if affected_rows != 1
end

# Assumes no slots yet exist
def add_effect_slots_for_player(player_id)
  client = connect_to_db(@server_database)
  (1..20).each do |slot|
    query = "INSERT [dbo].[PlayerEffects] ([PlayerID], [EffectSlot], [EffectID], [Amount], \
      [Duration]) \
      VALUES (#{player_id}, #{slot}, 0, 0, 0)"
    result = client.execute(query)
    affected_rows = result.do
    fail 'Unable to insert effect slot!' if affected_rows != 1
  end
end

def find_empty_effect_slot(player_id)
  query = "SELECT * FROM [#{@server_database}].[dbo].[PlayerEffects] \
           WHERE [PlayerID] = '#{player_id}' AND [EffectID] = 0"
  client = connect_to_db(@server_database)
  debug_msg query
  result = client.execute(query)
  row = result.each(first: true).first
  fail "No effect slot available for player #{player_id}" if row.nil?

  debug_msg "Next effect slot for player #{player_id}: #{row['EffectSlot']}"
  row['EffectSlot']
end

# CREATE TABLE [dbo].[PlayerEffects](
#   [EffectListID] [int] IDENTITY(1,1) NOT NULL,
#   [PlayerID] [int] NOT NULL,
#   [EffectSlot] [int] NULL,
#   [EffectID] [int] NULL,
#   [Amount] [int] NULL,
#   [Duration] [int] NULL
# ) ON [PRIMARY]
# GO
# SET IDENTITY_INSERT [dbo].[PlayerEffects] ON

# INSERT [dbo].[PlayerEffects] ([EffectListID], [PlayerID], [EffectSlot], [EffectID], [Amount],
#  [Duration]) VALUES (01, 1, 1, 0, 0, 0)
