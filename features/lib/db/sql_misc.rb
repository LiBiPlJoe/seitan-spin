# frozen_string_literal: true

require 'date'

TIME_OFFSETS = {
  now: 0,
  yesterday: -24 * 60 * 60,
  last_week: -7 * 24 * 60 * 60,
  last_year: -365 * 24 * 60 * 60
}.freeze

def db_insert_from_hash(table_name, db_hash)
  field_names = []
  values = []
  db_special_values(db_hash).each_pair do |key, value|
    field_names << "[#{key}]"
    values << db_string(value)
  end
  "INSERT [dbo].[#{table_name}] (#{field_names.join(', ')}) " \
    "VALUES (#{values.join(', ')})"
end

def db_field_from_hash(table_name, db_hash, return_field)
  where_clauses = []
  db_special_values(db_hash).each_pair do |key, value|
    where_clauses << "#{key}=#{db_string(value)}"
  end
  "SELECT #{return_field} FROM [dbo].[#{table_name}] " \
    "WHERE #{where_clauses.join(' AND ')}"
end

def quote_if_string(value)
  return "'#{value}'" if value.is_a?(String)

  value.to_s
end

def db_string(value)
  if value.is_a?(String)
    "'#{value}'"
  elsif value.nil?
    'NULL'
  else
    value.to_s
  end
end

def db_truefalse(value)
  case value
  when false
    0
  when true
    1
  else
    value
  end
end

def db_reltime(value)
  if TIME_OFFSETS.key?(value)
    (Time.now + TIME_OFFSETS[value]).strftime('%Y-%m-%d %H:%M:%S.%L')
  else
    value
  end
end

def db_special_values(db_hash)
  new_hash = {}
  db_hash.each_pair do |key, value|
    new_hash[key] = db_reltime(db_truefalse(value))
    debug_msg "#{value.inspect} --> #{new_hash[key].inspect}" if value != new_hash[key]
  end
  new_hash
end
