# frozen_string_literal: true

PLAYER_TABLES = %w[
  PlayerBelt PlayerEffects PlayerFlags PlayerHeld PlayerLocker PlayerQuests PlayerRings PlayerSack
  PlayerSettings PlayerSkills PlayerSpells PlayerWearing Player
].freeze

DEFAULT_PLAYER = {
  accountID: 0, account: 'no_name', name: 'no_name', gender: 1, race: 'Illyria',
  classFullName: 'Fighter', classType: 'Fighter', visualKey: 'male_fighter_pc_brown', alignment: 1,
  confRoom: 0, impLevel: 0, ancestor: 0, ancestorID: 0, facet: 0, land: 0, map: 0, xCord: 42,
  yCord: 27, zCord: 0, dirPointer: 'v', stunned: 0, floating: 3, dead: 0,
  fighterSpecialization: 'None', level: 4, exp: 4996, hits: 36, hitsMax: 36, hitsAdjustment: 0,
  hitsDoctored: 0, stamina: 10, stamLeft: 10, staminaAdjustment: 0, mana: 0, manaMax: 0,
  manaAdjustment: 0, age: 806, roundsPlayed: 806, numKills: 8, numDeaths: 0, bankGold: 0,
  strength: 17, dexterity: 18, intelligence: 10, wisdom: 17, constitution: 8, charisma: 18,
  strengthAdd: 1, dexterityAdd: 1, birthday: :last_week, lastOnline: :yesterday,
  deleteDate: :last_year, currentKarma: 0, lifetimeKarma: 0, lifetimeMarks: 0, pvpKills: 0,
  pvpDeaths: 0, playersKilled: '', playersFlagged: '', UW_hitsMax: 0, UW_hitsAdjustment: 0,
  UW_staminaMax: 0, UW_staminaAdjustment: 0, UW_manaMax: 0, UW_manaAdjustment: 0,
  UW_intestines: 0, UW_liver: 0, UW_lungs: 0, UW_stomach: 0
}.freeze

DEFAULT_PLAYER_SKILLS = {
  mace: 0, bow: 0, flail: 0, dagger: 0, rapier: 0, twoHanded: 0, staff: 0, shuriken: 0, sword: 0,
  threestaff: 0, halberd: 0, unarmed: 0, thievery: 0, magic: 0, bash: 0, highMace: 0, highBow: 0,
  highFlail: 0, highDagger: 0, highRapier: 0, highTwoHanded: 0, highStaff: 0, highShuriken: 0,
  highSword: 0, highThreestaff: 0, highHalberd: 0, highUnarmed: 0, highThievery: 0, highMagic: 0,
  highBash: 0, trainedMace: 0, trainedBow: 0, trainedFlail: 0, trainedDagger: 0, trainedRapier: 0,
  trainedTwoHanded: 0, trainedStaff: 0, trainedShuriken: 0, trainedSword: 0, trainedThreestaff: 0,
  trainedHalberd: 0, trainedUnarmed: 0, trainedThievery: 0, trainedMagic: 0, trainedBash: 0
}.freeze

DEFAULT_PLAYER_HAND = {
  playerID: 0, rightHand: 0, itemID: 0, attunedID: 0, special: '', charges: 0, coinValue: 0,
  figExp: 0, venom: 0, attuneType: 0, nocked: 0, timeCreated: :now, whoCreated: 'SYSTEM'
}.freeze

def db_insert_player(db_hash)
  debug_msg player = DEFAULT_PLAYER.merge(db_hash) # anything in the hash overrides defaults
  client = connect_to_db(@server_database)
  debug_msg query = db_insert_from_hash('Player', player)
  result = client.execute(query)
  affected_rows = result.do
  fail 'Unable to insert player!' if affected_rows != 1

  add_misc_to_player(get_player_id(player[:name]))
end

def add_misc_to_player(player_id)
  add_spell_slots_for_player(player_id)
  add_hand_slots_for_player(player_id)
  add_effect_slots_for_player(player_id)
  add_skills_for_player(player_id)
end

def player_exists(player_hash)
  client = connect_to_db(@server_database)
  conditions = []
  player_hash.each_key do |key|
    conditions << "#{key} = #{quote_if_string(player_hash[key])}"
  end
  debug_msg query = "SELECT [name], [playerID] FROM [#{@server_database}].[dbo].[Player] WHERE " +
                    conditions.join(' AND ')
  result = client.execute(query)
  debug_msg result.each(first: true)
  result.affected_rows.positive?
end

def delete_all_test_players
  client = connect_to_db(@server_database)
  all_test_player_ids.each do |player_to_delete|
    debug_msg "Deleting player #{player_to_delete}"
    PLAYER_TABLES.each do |table|
      result = client.execute("DELETE FROM [#{@server_database}].[dbo].[#{table}] \
                               WHERE playerID = '#{player_to_delete}'")
      debug_msg "Rows deleted from [#{table}]: #{result.do}"
    end
  end
end

def all_test_player_ids
  id_list = []
  client = connect_to_db(@server_database)
  query = "SELECT [playerID] FROM [#{@server_database}].[dbo].[Player] \
           WHERE account LIKE 'test%' \
           AND name LIKE 'Dude.%'"
  result = client.execute(query)
  result.each(symbolize_keys: true) do |rowset|
    id_list << rowset[:playerID]
  end
  id_list
end

def get_player_id(name)
  debug_msg "Getting ID from player #{name}"
  client = connect_to_db(@server_database)
  query = "SELECT [name], [playerID] FROM [#{@server_database}].[dbo].[Player] \
             WHERE name = N'#{name}'"
  result = client.execute(query)
  row = result.each(first: true)
  debug_msg row.inspect
  fail 'Unable to get playerID!' if result.affected_rows != 1

  debug_msg "Player ID for #{name} is #{row[0]['playerID']}"
  row[0]['playerID']
end

def db_make_player_thief(name)
  client = connect_to_db(@server_database)
  query = "UPDATE [#{@server_database}].[dbo].[Player] SET classFullName = N'Thief', \
    classType = N'Thief', visualKey = N'male_thief_pc_brown', alignment = 2, mana = 10, \
    manaMax = 10 WHERE name = '#{name}'"
  debug_msg query
  result = client.execute(query)
  affected_rows = result.do
  fail 'Unable to update player!' if affected_rows != 1
end

def move_player_to(player_id, x, y, z)
  client = connect_to_db(@server_database)
  query = "UPDATE [#{@server_database}].[dbo].[Player] SET \
    xCord = #{x}, yCord = #{y}, zCord = #{z} \
    WHERE playerID = #{player_id}"
  debug_msg query
  result = client.execute(query)
  affected_rows = result.do
  fail 'Unable to update player!' if affected_rows != 1
end

## PlayerSkills
def add_skills_for_player(player_id)
  client = connect_to_db(@server_database)
  player_skills = DEFAULT_PLAYER_SKILLS.merge({ playerID: player_id })
  debug_msg player_skills
  query = db_insert_from_hash('PlayerSkills', player_skills)
  debug_msg query
  result = client.execute(query)
  affected_rows = result.do
  fail 'Unable to insert player skills!' if affected_rows != 1
end

## PlayerHeld

# Assumes no slots yet exist
def add_hand_slots_for_player(player_id)
  client = connect_to_db(@server_database)
  2.times do |hand|
    debug_msg query = db_insert_from_hash('PlayerHeld',
                                          DEFAULT_PLAYER_HAND.merge({ playerID: player_id,
                                                                      rightHand: hand }))
    # query = "INSERT [dbo].[PlayerHeld] ([playerID], [rightHand], [itemID], [attunedID], \
    #   [special], [charges], [coinValue], [figExp], [venom], [attuneType], [nocked], \
    #   [timeCreated], [whoCreated]) \
    #   VALUES (#{player_id}, #{hand}, 0, 0, N'', 0, 0, 0, 0, 0, 0, (CURRENT_TIMESTAMP), \
    #   N'SYSTEM')"
    result = client.execute(query)
    affected_rows = result.do
    fail 'Unable to insert hand slot!' if affected_rows != 1
  end
end

def clear_player_hands
  client = connect_to_db(@server_database)
  query = "UPDATE [dbo].[PlayerHeld] SET \
    itemID = 0, attunedID = 0, special = ''"
  debug_msg query
  result = client.execute(query)
  affected_rows = result.do
  debug_msg "Cleared #{affected_rows} player hands"
end

def put_in_player_hand(player_id, item_id, which_hand = :right)
  client = connect_to_db(@server_database)
  special = lookup_item_special({ itemID: item_id })
  query = "UPDATE [#{@server_database}].[dbo].[PlayerHeld] SET \
    itemID = #{item_id}, special = '#{special}' \
    WHERE playerID = #{player_id} AND rightHand = #{which_hand == :right ? 1 : 0}"
  debug_msg query
  result = client.execute(query)
  affected_rows = result.do
  fail 'Unable to update player!' if affected_rows != 1
end

def put_attuned_in_player_hand(player_id, item_id, which_hand = :right)
  client = connect_to_db(@server_database)
  query = "UPDATE [#{@server_database}].[dbo].[PlayerHeld] SET \
    itemID = #{item_id}, attunedID = #{player_id} \
    WHERE playerID = #{player_id} AND rightHand = #{which_hand == :right ? 1 : 0}"
  debug_msg query
  result = client.execute(query)
  affected_rows = result.do
  fail 'Unable to update player!' if affected_rows != 1
end

## Attributes

def set_character_alignment(player_id, desired_alignment)
  alignment_list = %i[none lawful neutral chaotic evil amoral all]
  alignment_num = alignment_list.index(desired_alignment)
  debug_msg "Alignment #{desired_alignment} is number #{alignment_num}"
  fail 'Unknown alignment' if alignment_num.nil?

  client = connect_to_db(@server_database)
  query = "UPDATE [#{@server_database}].[dbo].[Player] SET \
    alignment = #{alignment_num} \
    WHERE playerID = '#{player_id}'"
  debug_msg query
  result = client.execute(query)
  affected_rows = result.do
  fail 'Unable to update player!' if affected_rows != 1
end

def set_character_karma(player_id, desired_karma)
  client = connect_to_db(@server_database)
  query = "UPDATE [#{@server_database}].[dbo].[Player] SET \
    currentKarma = #{desired_karma} \
    WHERE playerID = '#{player_id}'"
  debug_msg query
  result = client.execute(query)
  affected_rows = result.do
  fail 'Unable to update player!' if affected_rows != 1
end

def set_character_stat(player_id, stat, desired_value)
  client = connect_to_db(@server_database)
  query = "UPDATE [#{@server_database}].[dbo].[Player] SET \
    #{stat} = #{desired_value} \
    WHERE playerID = '#{player_id}'"
  debug_msg query
  result = client.execute(query)
  affected_rows = result.do
  fail 'Unable to update player!' if affected_rows != 1
end

def set_character_skill(player_id, skill, desired_value)
  client = connect_to_db(@server_database)
  query = "UPDATE [#{@server_database}].[dbo].[PlayerSkills] SET \
    #{skill} = #{desired_value} \
    WHERE playerID = '#{player_id}'"
  debug_msg query
  result = client.execute(query)
  affected_rows = result.do
  fail 'Unable to update player skill!' if affected_rows != 1
end

def set_character_max_hp(player_id, desired_hp)
  client = connect_to_db(@server_database)
  query = "UPDATE [#{@server_database}].[dbo].[Player] SET \
    hitsMax = #{desired_hp} \
    WHERE playerID = '#{player_id}'"
  debug_msg query
  result = client.execute(query)
  affected_rows = result.do
  fail 'Unable to update player!' if affected_rows != 1
end

def set_character_hp(player_id, desired_hp)
  client = connect_to_db(@server_database)
  query = "UPDATE [#{@server_database}].[dbo].[Player] SET \
    hits = #{desired_hp} \
    WHERE playerID = '#{player_id}'"
  debug_msg query
  result = client.execute(query)
  affected_rows = result.do
  fail 'Unable to update player!' if affected_rows != 1
end

def set_character_max_stamina(player_id, desired_value)
  client = connect_to_db(@server_database)
  query = "UPDATE [#{@server_database}].[dbo].[Player] SET \
    stamina = #{desired_value} \
    WHERE playerID = '#{player_id}'"
  debug_msg query
  result = client.execute(query)
  affected_rows = result.do
  fail 'Unable to update player!' if affected_rows != 1
end

def set_character_stamina(player_id, desired_value)
  client = connect_to_db(@server_database)
  query = "UPDATE [#{@server_database}].[dbo].[Player] SET \
    stamLeft = #{desired_value} \
    WHERE playerID = '#{player_id}'"
  debug_msg query
  result = client.execute(query)
  affected_rows = result.do
  fail 'Unable to update player!' if affected_rows != 1
end

def set_character_max_mana(player_id, desired_value)
  client = connect_to_db(@server_database)
  query = "UPDATE [#{@server_database}].[dbo].[Player] SET \
    manaMax = #{desired_value} \
    WHERE playerID = '#{player_id}'"
  debug_msg query
  result = client.execute(query)
  affected_rows = result.do
  fail 'Unable to update player!' if affected_rows != 1
end

def set_character_mana(player_id, desired_value)
  client = connect_to_db(@server_database)
  query = "UPDATE [#{@server_database}].[dbo].[Player] SET \
    mana = #{desired_value} \
    WHERE playerID = '#{player_id}'"
  debug_msg query
  result = client.execute(query)
  affected_rows = result.do
  fail 'Unable to update player!' if affected_rows != 1
end

def set_character_constitution(player_id, desired_constitution)
  client = connect_to_db(@server_database)
  query = "UPDATE [#{@server_database}].[dbo].[Player] SET \
    constitution = #{desired_constitution} \
    WHERE playerID = '#{player_id}'"
  debug_msg query
  result = client.execute(query)
  affected_rows = result.do
  fail 'Unable to update player!' if affected_rows != 1
end

def set_character_age(player_id, desired)
  client = connect_to_db(@server_database)
  query = "UPDATE [#{@server_database}].[dbo].[Player] SET \
    age = #{desired} \
    WHERE playerID = '#{player_id}'"
  debug_msg query
  result = client.execute(query)
  affected_rows = result.do
  fail 'Unable to update player!' if affected_rows != 1
end
