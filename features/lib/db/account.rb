# frozen_string_literal: true

def delete_all_test_accounts
  client = connect_to_db(@server_database)
  result = client.execute("DELETE
    FROM [#{@server_database}].[dbo].[Account]
    WHERE account LIKE 'test%'
    AND email LIKE '%301days.com'")
  debug_msg "Rows deleted from [Account]: #{result.do}"
end
