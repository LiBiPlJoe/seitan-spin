# frozen_string_literal: true

# Plan to build this out over time as necessary
DEFAULT_SPELL = {
  soundFile: nil
}.freeze

DEFAULT_PLAYERSPELL = {
  ChantString: 'na na na na'
}.freeze

# Assumes no slots yet exist
def add_spell_slots_for_player(player_id)
  client = connect_to_db(@server_database)
  (1..35).each do |spell_slot|
    query = "INSERT [#{@server_database}].[dbo].[PlayerSpells] ([PlayerID], [SpellSlot], \
             [SpellID], [ChantString]) \
             VALUES (#{player_id}, #{spell_slot}, -1, NULL)"
    result = client.execute(query)
    affected_rows = result.do
    fail 'Unable to insert spell slot!' if affected_rows != 1
  end
end

# INSERT [dbo].[PlayerSpells] ([SpellListID], [PlayerID], [SpellSlot], [SpellID], [ChantString]) \
# VALUES (26, 1, 1, -1, NULL)

def ensure_player_has_spell(player_id, spell_id, chant_string = DEFAULT_PLAYERSPELL[:ChantString])
  return if does_player_have_spell?(player_id, spell_id, chant_string)

  add_spell_to_player(player_id, spell_id, chant_string)
end

def does_player_have_spell?(player_id, spell_id, chant_string)
  client = connect_to_db(@server_database)
  query = "SELECT * FROM [#{@server_database}].[dbo].[PlayerSpells] \
             WHERE PlayerID = #{player_id} AND SpellID = #{spell_id} \
             AND ChantString = '#{chant_string}'"
  result = client.execute(query)
  row = result.each(first: true).first
  debug_msg row.inspect
  false if result.affected_rows < 1
end

def find_empty_spell_slot(player_id)
  client = connect_to_db(@server_database)
  query = "SELECT * FROM [#{@server_database}].[dbo].[PlayerSpells] \
             WHERE PlayerID = #{player_id} AND SpellID = -1"
  result = client.execute(query)
  row = result.each(first: true).first
  debug_msg row.inspect
  fail "No empty spell slots on player #{player_id}" if result.affected_rows < 1

  row['SpellSlot']
end

def add_spell_to_player(player_id, spell_id, chant_string = DEFAULT_PLAYERSPELL[:ChantString])
  empty_slot = find_empty_spell_slot(player_id)
  client = connect_to_db(@server_database)
  query = "UPDATE [#{@server_database}].[dbo].[PlayerSpells] \
    SET SpellID = #{spell_id}, ChantString = N'#{chant_string}' \
    WHERE PlayerID = #{player_id} AND SpellSlot = #{empty_slot}"
  result = client.execute(query)
  affected_rows = result.do
  fail 'Unable to update PlayerSpells!' if affected_rows != 1

  empty_slot # Return the slot where the spell was placed if successful
end

def get_spell_id(spell_command)
  client = connect_to_db(@server_database)
  query = "SELECT [command], [spellID] FROM [#{@server_database}].[dbo].[Spells] \
             WHERE command = '#{spell_command}'"
  result = client.execute(query)
  row = result.each(first: true)
  debug_msg row.inspect
  fail 'Unable to get spellID!' if result.affected_rows != 1

  row[0]['spellID']
end
